import React from "react";
import { Button } from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";

const CustomButton = (props) => {
  const { title } = props;
  return (
    <Button type="primary" icon={<PlusCircleOutlined />}>
      {title}
    </Button>
  );
};

export default CustomButton;
