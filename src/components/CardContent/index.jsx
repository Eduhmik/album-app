import React from "react";
import { Table } from "antd";

const CardContent = (props) => {
  const { tracks } = props;
  const columns = [
    {
      title: "#",
      dataIndex: "number",
      key: "number",
    },
    {
      title: "Track Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Track Length",
      dataIndex: "duration",
      key: "duration",
    },
  ];
  const format = (data) => {
    return data.map((element, index) => ({
      ...element,
      number: index + 1,
    }));
  };

  return <Table columns={columns} dataSource={format(tracks)} />;
};

export default CardContent;
