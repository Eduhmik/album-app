import React from "react";
import { Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import { FolderOpenOutlined, UserOutlined } from "@ant-design/icons";
import "../../App.css";

const { Header, Content, Footer, Sider } = Layout;

const MainLayout = (props) => {
  const { children } = props;

  return (
    <Layout>
      <Header className="header">
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["1"]}
          style={{
            lineHeight: "64px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Menu.Item key="1">Virtual Album Shelf</Menu.Item>
        </Menu>
      </Header>

      <Layout>
        <Sider width={200} style={{ background: "#fff" }}>
          <Menu
            mode="inline"
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            style={{ height: "100%", borderRight: 0 }}
          >
            <Menu.Item key="sub1" icon={<FolderOpenOutlined />}>
              <Link to="/">Albums</Link>
            </Menu.Item>
            <Menu.Item key="sub2" icon={<UserOutlined />}>
              <Link to="/artist">Artists</Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout style={{ padding: "0 24px 24px" }}>
          <Content
            style={{
              background: "#fff",
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
      <Footer style={{ textAlign: "center" }}>
        Album Design ©2021 Created by Eduhmik
      </Footer>
    </Layout>
  );
};

export default MainLayout;
