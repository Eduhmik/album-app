import React from "react";
import { Skeleton, Card, Avatar, Collapse } from "antd";
import { DeleteOutlined } from "@ant-design/icons";
import CardContent from "../CardContent";
import styles from "./styles.module.scss";

const { Meta } = Card;
const Panel = Collapse.Panel;

const AlbumItem = (props) => {
  const { data, isLoading } = props;
  const { name, artistName, images, tracks } = data;

  return (
    <>
      <Card
        hoverable
        style={{ maxWidth: "100%", marginTop: 16 }}
        extra={<DeleteOutlined />}
      >
        <Skeleton loading={isLoading} avatar active>
          <Meta
            avatar={
              <Avatar
                shape="square"
                size="large"
                style={{ width: 100, height: 100 }}
                src={images ? images[0] : ""}
              />
            }
            title={name}
            description={`By ${artistName}`}
          />
        </Skeleton>
        <Collapse
          className={styles.collapse}
          bordered={false}
          defaultActiveKey={[]}
        >
          <Panel header={`View Track List (${tracks?.length})`} key="1">
            <CardContent tracks = {tracks}/>
          </Panel>
        </Collapse>
      </Card>
    </>
  );
};

export default AlbumItem;
