import React from "react";
import { Checkbox } from "antd";

function onChange(checkedValues) {
  console.log("checked = ", checkedValues);
}

const CheckBox = () => {
  const options = [
    { label: "Album", value: "Album" },
    { label: "Artist", value: "Artist" },
  ];

  return (
    <>
      <Checkbox.Group
        options={options}
        defaultValue={["Album"]}
        onChange={onChange}
      />
    </>
  );
};

export default CheckBox;
