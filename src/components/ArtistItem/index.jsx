import React from "react";
import { Card, Avatar, Skeleton} from "antd";
import { EditOutlined, EllipsisOutlined } from "@ant-design/icons";

const { Meta } = Card;

const ArtistItem = (props) => {
  const { data, isLoading } = props;
  const { name, listeners, images } = data;
  return (
    <Card
      style={{ width: "100%" }}
      hoverable
      actions={[
        <EditOutlined key="edit" />,
        <EllipsisOutlined key="ellipsis" />,
      ]}
    >
      <Skeleton loading={isLoading} avatar active>
        <Meta
          avatar={
            <Avatar src={images ? images[1] : ""} />
          }
          title={name}
          description={`Listeners | ${listeners}`}
        />
      </Skeleton>
    </Card>
  );
};

export default ArtistItem;
