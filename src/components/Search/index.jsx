import React from "react";
import { Input, AutoComplete } from "antd";

const Complete = (props) => {
  const { onChange } = props;

  return (
    <AutoComplete
      dropdownMatchSelectWidth={252}
      style={{
        width: 300,
      }}
      onChange={onChange}
    >
      <Input.Search size="large" placeholder="Search here" enterButton />
    </AutoComplete>
  );
};

export default Complete;
