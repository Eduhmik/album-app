import {
    FETCH_ARTIST,
    FETCH_ARTIST_SUCCESS,
    FETCH_ARTIST_FAILURE,
  } from "../constants/actionTypes";

  // const artists = [
  //   {
  //     id: "70fac1fa-9ed1-43bc-92f0-0032ea140fe8",
  //     name: "Pink Floyd"
  //   },
  //   {
  //     id: "3a82d368-afcb-4e54-b678-f1b0e0828703",
  //     name: "King Crimson"
  //   },
  //   {
  //     id: "8542f5f4-ef70-43e9-9d58-99ec50b3ab6b",
  //     name: "Sonic Youth"
  //   },
  //   {
  //     id: "4d2e0178-ac33-4c6c-a568-8214410c20b1", 
  //     name: "Yes"
  //   },
  //   {
  //     id: "984ee0e0-1f01-4855-853a-ebc3a372653d", 
  //     name: "John Frusciante"
  //   },
  //   {
  //     id: "a452070e-1507-4a77-9373-e76085ad231c", 
  //     name: "Manogurgeil"
  //   },
  //   {
  //     id: "805aa824-bab2-487d-98f4-daede3e772cc",
  //     name: "Butthole Surfers"
  //   }
  // ]
  
  const initialState = {
    isLoading: false,
    data: [],
    error: null,
  };
  
  const albumReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_ARTIST:
        return {
          ...state,
          isLoading: true,
          error: null,
        };
      case FETCH_ARTIST_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: action.data.result,
          error: null,
        };
      case FETCH_ARTIST_FAILURE:
        return {
          ...state,
          isLoading: false,
          error: action.error,
        };
      default:
        return state;
    }
  };
  
  export default albumReducer;