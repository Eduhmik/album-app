import { combineReducers } from "redux";
import albumReducer from "./albumReducer";
import artistReducer from "./artistReducer";
import searchAlbumReducer from "./searchAlbumReducer";

const rootReducer = combineReducers({
  albumReducer,
  artistReducer,
  searchAlbumReducer,
});

export default rootReducer;
