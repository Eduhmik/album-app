import {
    SEARCH_ALBUM,
    SEARCH_ALBUM_SUCCESS,
    SEARCH_ALBUM_FAILURE,
  } from "../constants/actionTypes";
  
  const initialState = {
    loading: false,
    searchData: [],
    error: null,
  };
  
  const searchAlbumReducer = (state = initialState, action) => {
    switch (action.type) {
      case SEARCH_ALBUM:
        return {
          ...state,
          loading: true,
          error: null,
        };
      case SEARCH_ALBUM_SUCCESS:
        return {
          ...state,
          loading: false,
          searchData: action.data,
          error: null,
        };
      case SEARCH_ALBUM_FAILURE:
        return {
          ...state,
          loading: false,
          searchData: [],
          error: action.error,
        };
      default:
        return state;
    }
  };
  
  export default searchAlbumReducer;