import {
  FETCH_ALBUM,
  FETCH_ALBUM_SUCCESS,
  FETCH_ALBUM_FAILURE,
} from "../constants/actionTypes";

const initialState = {
  isLoading: false,
  data: [],
  error: null,
};

const albumReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUM:
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    case FETCH_ALBUM_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.data,
        error: null,
      };
    case FETCH_ALBUM_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default albumReducer;
