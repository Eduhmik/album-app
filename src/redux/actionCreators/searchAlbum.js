import {
    SEARCH_ALBUM,
    SEARCH_ALBUM_SUCCESS,
    SEARCH_ALBUM_FAILURE,
  } from "../constants/actionTypes";
  
  export const searchAlbum = () => ({ type: SEARCH_ALBUM });
  
  
  export const searchAlbumSuccess = (data) => ({
      type: SEARCH_ALBUM_SUCCESS,
      data,
  });
  
  export const searchAlbumFailure = (error) => ({
      type: SEARCH_ALBUM_FAILURE,
      error,
  });
  