import {
  FETCH_ALBUM,
  FETCH_ALBUM_SUCCESS,
  FETCH_ALBUM_FAILURE,
} from "../constants/actionTypes";

export const fetchAlbum = () => ({ type: FETCH_ALBUM });


export const fetchAlbumSuccess = (data) => ({
    type: FETCH_ALBUM_SUCCESS,
    data,
});

export const fetchAlbumFailure = (error) => ({
    type: FETCH_ALBUM_FAILURE,
    error,
});
