import {
    FETCH_ARTIST,
    FETCH_ARTIST_SUCCESS,
    FETCH_ARTIST_FAILURE,
  } from "../constants/actionTypes";
  
  export const fetchArtist = () => ({ type: FETCH_ARTIST });
  
  
  export const fetchArtistSuccess = (data) => ({
      type: FETCH_ARTIST_SUCCESS,
      data,
  });
  
  export const fetchArtistFailure = (error) => ({
      type: FETCH_ARTIST_FAILURE,
      error,
  });
  