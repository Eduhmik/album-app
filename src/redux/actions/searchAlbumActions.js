import {
  searchAlbum as searchAlbumAction,
  searchAlbumSuccess,
  searchAlbumFailure,
} from "../actionCreators/searchAlbum";
import LastFM from "last-fm";

const lastfm = new LastFM("00bc38bb48deb9ffb3da2f4b4d689741", {
  userAgent: "MyAlbumApp/1.0.0 (http://example.com)",
});

export const searchAlbum = (action) => async (dispatch) => {
  const albumsData = [];
  dispatch(
    searchAlbumAction(
      lastfm.albumInfo({ name: action, artistName: action }, (err, data) => {
        if (err) {
          dispatch(searchAlbumFailure(err));
        } else {
          albumsData.push(data);
          dispatch(searchAlbumSuccess(albumsData));
        }
      })
    )
  );
};
