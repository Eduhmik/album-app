import {
  fetchArtist as fetchArtistAction,
  fetchArtistSuccess,
  fetchArtistFailure,
} from "../actionCreators/artist";
import LastFM from "last-fm";

const lastfm = new LastFM("00bc38bb48deb9ffb3da2f4b4d689741", {
  userAgent: "MyAlbumApp/1.0.0 (http://example.com)",
});

export const fetchArtist = (action) => async (dispatch) => {
  dispatch(
    fetchArtistAction(
      lastfm.artistSearch({ q: "K" }, (err, data) => {
        if (err) dispatch(fetchArtistFailure(err));
        else dispatch(fetchArtistSuccess(data));
      })
    )
  );
};
