import {
  fetchAlbum as fetchAlbumAction,
  fetchAlbumSuccess,
  fetchAlbumFailure,
} from "../actionCreators/album";
import LastFM from "last-fm";

const lastfm = new LastFM("00bc38bb48deb9ffb3da2f4b4d689741", {
  userAgent: "MyAlbumApp/1.0.0 (http://example.com)",
});

export const fetchAlbum = () => async (dispatch) => {
  const albumsData = [];

  dispatch(
    fetchAlbumAction(
      lastfm.albumSearch({ q: "The", limit: 20 }, (err, data) => {
        if (err) dispatch(fetchAlbumFailure(err));
        else {
          return data?.result.map((albumData) =>
            lastfm.albumInfo(
              { name: albumData.name, artistName: albumData.artistName },
              (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  albumsData.push(data);
                  dispatch(fetchAlbumSuccess(albumsData));
                }
              }
            )
          );
        }
      })
    )
  );
};
