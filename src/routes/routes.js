import Home from "../containers/Home"
import Artist from "../containers/Artist"

const routes = [
    {
        path: "/",
        component:Home,
        id: 'home',
        exact: true
    },
    {
        path: "/artist",
        component:Artist,
        id: 'artist',
        exact: true
    }
]

export default routes;