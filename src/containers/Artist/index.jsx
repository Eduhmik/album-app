import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Col, Row } from "antd";
import { fetchArtist as fetchArtistAction } from "../../redux/actions/artistActions";
import ArtistItem from "../../components/ArtistItem";
import Button from "../../components/Button";
import Search from "../../components/Search";

const Artist = () => {
  const { data, isLoading } = useSelector((state) => state.artistReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchArtistAction());
  }, []);

  return (
    <div>
      <Row justify="space-around" align="bottom">
        <Col span={6}>
          <Search />
        </Col>
        <Col span={6}>
          <Button title={"Add Artist"} />
        </Col>
      </Row>

      <Row style={{ marginTop: 50 }}>
        <Col span={6} offset={12}>
          <h2>Artists</h2>
        </Col>
      </Row>

      <Row gutter={16}>
        {data?.map((artistData) => (
          <Col xs={24} sm={24} md={12} lg={12} style={{ marginTop: 10 }}>
            <ArtistItem data={artistData} isLoading={isLoading} />
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default Artist;
