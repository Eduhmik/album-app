import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Col, Row } from "antd";
import { fetchAlbum as fetchAlbumAction } from "../../redux/actions/albumActions";
import { searchAlbum as searchAlbumAction } from "../../redux/actions/searchAlbumActions";
import { searchAlbumSuccess } from "../../redux/actionCreators/searchAlbum";

import Button from "../../components/Button";
import Search from "../../components/Search";
import Checkbox from "../../components/Checkbox";
import AlbumItem from "../../components/AlbumItem";

const Home = () => {
  const { data, isLoading } = useSelector((state) => state.albumReducer);
  const { searchData } = useSelector((state) => state.searchAlbumReducer);

  const dispatch = useDispatch();

  const [search, setSearch] = useState("");

  useEffect(() => {
    dispatch(fetchAlbumAction());
  }, []);

  useEffect(() => {
    if (search !== "") {
      dispatch(searchAlbumAction(search));
    } else {
      dispatch(searchAlbumSuccess([]));
    }
  }, [search]);

  const onChange = (value) => {
    setSearch(value?.trim());
  };

  console.log(searchData, "searchData");
  return (
    <div>
      <Row justify="space-around" align="bottom">
        <Col span={6}>
          <Search onChange={onChange} />
        </Col>
        <Col span={6}>
          <Checkbox />
        </Col>
        <Col span={6}>
          <Button title={"Add New Album"} />
        </Col>
      </Row>
      <Row style={{ marginTop: 50 }}>
        <Col span={6} offset={12}>
          <h2>Albums</h2>
        </Col>
      </Row>
      {searchData.length === 0 && search.trim() === "" ? (
        <Row gutter={16} style={{ marginTop: 30 }}>
          {data?.map((albumData) => (
            <Col xs={24} sm={24} md={12} lg={12} style={{ marginTop: 20 }}>
              <AlbumItem data={albumData} isLoading={isLoading} />
            </Col>
          ))}
        </Row>
      ) : (
        <Row gutter={16} style={{ marginTop: 30 }}>
          {searchData?.map((albumData) => (
            <Col xs={24} sm={24} md={12} lg={12} style={{ marginTop: 20 }}>
              <AlbumItem data={albumData} isLoading={isLoading} />
            </Col>
          ))}
        </Row>
      )}
    </div>
  );
};

export default Home;
